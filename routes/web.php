<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;


Route::get('/', [OrderController::class, 'main']);

Auth::routes();
Route::resource('orders', OrderController::class);
