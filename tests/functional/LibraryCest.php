<?php

class LibraryCest
{
    public function _before(FunctionalTester $I)
    {
        $I->amOnPage('/login');
        $I->fillField('input[name=email]','orishchak.b@ukr.net');
        $I->fillField('input[name=password]','123456789');
        $I->click('Login');
    }

    // tests
    public function tryToTest(FunctionalTester $I)
    {
        $I->amOnPage('/orders');
        $I->canSee('Адмінка');
    }
    // tests
    public function testgetBookByTitle(FunctionalTester $I)
    {
        $I->haveInDatabase('orders',
            array('book_title' => 'Гаррі Потер та камінь Php',
                'session_start' => '2021-05-12 00:00:00',
                'user_fio' => 'Молодий Вова'));

        $I->seeInDatabase('orders', ['book_title' => 'Гаррі Потер та камінь PhP']);
    }
}
