@extends('layouts.app')
@section('content')
    <div class="row justify-content-center" xmlns="http://www.w3.org/1999/html">
        <div class="col col-3">
<h1>Інформація:</h1>
<b>ID: <i>{{$order->order_id}}</i></b><br>
<b>Назва книги: <i>{{$order->book_title}}</i></b><br>
<b>ФІО користувача: <i>{{$order->user_fio}}</i></b><br>
<b>Початок сесії: <i>{{$order->session_start}}</i></b><br>
<a href="/orders">Повернутись</a>
        </div>
    </div>
@endsection
