@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        <div class="col col-4">
    <h1>Інформаційна система бібліотека</h1>
<div class="text-lg-center">
    <a href="/orders" style="padding-right: 100px">Адмінка</a>
    <a href="/login" style="padding-right: 100px">Увійти</a>
    <a href="/register" style="padding-right: 40px">Зареєструватись</a>
</div>


    <table border="1" style="margin-top: 15px">
        <tr>
            <td>ID</td>
            <td>Назва книги</td>
            <td>ПІБ читача</td>
            <td>Початок сесії</td>
        </tr>
        @foreach($orders as $order)
            <tr>
                <td>{{$order->order_id}}</td>
                <td><a href="/orders/{{$order->order_id}}">{{$order->book_title}}</a></td>
                <td>{{$order->user_fio}}</td>
                <td>{{$order->session_start}}</td>
            </tr>
        @endforeach
    </table>
        </div>
    </div>
@endsection
