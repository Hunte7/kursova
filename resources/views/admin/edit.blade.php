@extends('layouts.app')
@section('content')
    <div class="row justify-content-center" xmlns="http://www.w3.org/1999/html">
        <div class="col col-3">
    <h1>Редагувати запис</h1>

<form action="/orders/{{$order->order_id}}" method="post">
    @csrf
    @method('PUT')
    <span>Назва книги:<input name="book_title" placeholder="Назва книги" value="{{$order->book_title}}"> </span> <br><br>

    <span>ФІО користувачв: <input type="text" name="user_fio" placeholder="ПІБ" value="{{$order->user_fio}}"></span> <br><br>
    <span>Початок сесії: <input type="datetime-local" name="session_start" value="{{$order->session_start}}"></span> <br><br>
    <input type="submit" value="Редагувати">
    <div style="margin-top: 15px">
        <a href="/orders" style="padding-right: 200px">Назад</a>
    </div>
</form>


        </div>
    </div>
@endsection
