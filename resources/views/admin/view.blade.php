@extends('layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col col-5">
        <div style="text-align: center">
        <h1>Адмінка</h1>
        </div>
        <form>
            <span>Пошук за назвою книги: <input type="text" name="title" placeholder="Назва"></span>

            <span>Сортувати:
        <select name="sort">
            <option value="">Не сортувати</option>
            @foreach(\App\Models\Order::SORT_TYPES as $key => $type)
                <option value="{{$key}}" {{($request->sort == $key) ? 'selected' : ''}}>{{$type}}</option>
            @endforeach
        </select>
    </span>

            <input type="submit" value="Відобразити">
        </form>
        <div style="margin-top: 15px">
        <a href="/orders" style="padding-right: 200px">Скинути фільтри</a>
        <a href="/" style="padding-right: 140px">На головну</a>
        <a href="/orders/create"style="padding-right: 90px">Додати замовлення</a>
        </div>

        <table border="1" style="margin-top: 30px">
            <tr>
                <td>ID</td>
                <td>Назва книги</td>
                <td>ПІБ читача</td>
                <td>Початок сесії</td>
            </tr>
            @foreach($orders as $order)
                <tr>
                    <td>{{$order->order_id}}</td>
                    <td><a href="/orders/{{$order->order_id}}">{{$order->book_title}}</a></td>
                    <td>{{$order->user_fio}}</td>
                    <td>{{$order->session_start}}</td>
                    <td><a href="/orders/{{$order->order_id}}/edit">Редагувати запис</a></td>
                    <form action="/orders/{{$order->order_id}}" method="post">
                        @csrf
                        @method('DELETE')
                        <td><input type="submit" value="Видалити запис"></td>
                    </form>
                </tr>
            @endforeach
        </table>
        <div style="margin-top: 15px">
        <form action="{{ route('logout') }}" method="POST" >
            @csrf
            <input type="submit" value="Вийти з аккаунту">
        </form>
        </div>

    </div>
</div>
@endsection
