@extends('layouts.app')
@section('content')

    <div class="row justify-content-center">
        <div class="col col-3">
            <h1>Додати замовлення</h1>

<form action="/orders" method="post">
    @csrf
    <span>Назва книги: <input type="text" name="book_title" placeholder="Назва книги"></span> <br><br>
    <span>ФІО користувача: <input type="text" name="user_fio" placeholder="ПІБ"></span> <br><br>
    <span>Початок сесії: <input type="datetime-local" name="session_start"></span> <br><br>
    <input type="submit" value="Додати">
    <div style="margin-top: 15px">
        <a href="/orders" style="padding-right: 200px">Назад</a>
    </div>
</form>
        </div>
    </div>
@endsection
