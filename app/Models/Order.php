<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public const SORT_TYPES = [
        '1' => 'За назвою книги',
        '2' => 'За часом початку сессії',
    ];

    public $timestamps = false;
    protected $primaryKey = 'order_id';
    public $incrementing = true;
    protected $table = 'orders';

    protected $fillable = [
        'book_title',
        'user_fio',
        'session_start',
    ];
}
